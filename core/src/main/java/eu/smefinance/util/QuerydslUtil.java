package eu.smefinance.util;

import com.querydsl.jpa.JPQLQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.Querydsl;

import javax.annotation.Nonnull;
import java.util.Collections;

public class QuerydslUtil {

    public static <T> Page<T> getPageData(@Nonnull final Querydsl querydsl, @Nonnull final JPQLQuery<T> query, @Nonnull final Pageable pageable) {
        long total = query.fetchCount();
        return new PageImpl<>(
                total > pageable.getOffset() ? querydsl.applyPagination(pageable, query).fetch() : Collections.emptyList(),
                pageable,
                total
        );
    }
}
