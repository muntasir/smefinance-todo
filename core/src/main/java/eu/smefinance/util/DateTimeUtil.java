package eu.smefinance.util;

import javax.annotation.Nonnull;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.*;
import java.util.Date;

public class DateTimeUtil {

    @Nonnull
    public static ZonedDateTime toZonedDateTime(@Nonnull final LocalDateTime localDateTime) {
        return localDateTime.atZone(ZoneId.systemDefault());
    }

    @Nonnull
    public static ZonedDateTime toZonedDateTime(@Nonnull final Date date) {
        return ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
    }

    @Nonnull
    public static LocalDateTime toLocalDateTime(@Nonnull final ZonedDateTime zonedDateTime) {
        return LocalDateTime.ofInstant(zonedDateTime.toInstant(), ZoneId.systemDefault());
        // return zonedDateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime();
    }

    @Nonnull
    public static LocalDateTime toLocalDateTime(@Nonnull final Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        // return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    @Nonnull
    public static LocalDate toLocalDate(@Nonnull final ZonedDateTime zonedDateTime) {
        return LocalDate.ofInstant(zonedDateTime.toInstant(), ZoneId.systemDefault());
        // return zonedDateTime.withZoneSameInstant(ZoneId.systemDefault()).toLocalDate();
    }

    @Nonnull
    public static LocalDate toLocalDate(@Nonnull final Date date) {
        return LocalDate.ofInstant(date.toInstant(), ZoneId.systemDefault());
        // return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    @Nonnull
    public static Date toDate(@Nonnull final LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    @Nonnull
    public static Date toDate(@Nonnull final LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }

    @Nonnull
    public static Timestamp getStartOfDate(@Nonnull final String date, @Nonnull final String dateFormat) throws ParseException {
        return Timestamp.valueOf(toLocalDateTime(new SimpleDateFormat(dateFormat).parse(date)).with(LocalTime.MIN));
    }

    @Nonnull
    public static Timestamp getEndOfDate(@Nonnull final String date, @Nonnull final String dateFormat) throws ParseException {
        return Timestamp.valueOf(toLocalDateTime(new SimpleDateFormat(dateFormat).parse(date)).with(LocalTime.MAX));
    }
}
