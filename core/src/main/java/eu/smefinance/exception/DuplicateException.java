package eu.smefinance.exception;

import javax.annotation.Nonnull;

public class DuplicateException extends RuntimeException {

    private static final long serialVersionUID = -5071298719204544838L;

    public DuplicateException(@Nonnull final String message) {
        super(message);
    }
}
