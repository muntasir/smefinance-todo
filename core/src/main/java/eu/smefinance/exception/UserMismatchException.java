package eu.smefinance.exception;

import eu.smefinance.model.BaseEntity;

import javax.annotation.Nonnull;

public class UserMismatchException extends RuntimeException {

    private static final long serialVersionUID = 992304872505849701L;

    public UserMismatchException(@Nonnull final Class<? extends BaseEntity> clazz) {
        super("This " + clazz.getSimpleName() + " does not belong to the user!");
    }
}
