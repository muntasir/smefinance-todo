package eu.smefinance.exception;

import eu.smefinance.model.BaseEntity;
import lombok.Getter;

@Getter
public class EntityNotFoundException extends RuntimeException {

    private static final long serialVersionUID = -6573603039916048722L;
    private final Class<? extends BaseEntity> clazz;

    public EntityNotFoundException(Class<? extends BaseEntity> clazz) {
        this.clazz = clazz;
    }

    @Override
    public String getMessage() {
        return clazz.getSimpleName() + " not found!";
    }
}
