package eu.smefinance.repository.impl;

import com.querydsl.core.BooleanBuilder;
import eu.smefinance.model.QTodo;
import eu.smefinance.model.Todo;
import eu.smefinance.model.User;
import eu.smefinance.repository.TodoRepositoryCustom;
import eu.smefinance.repository.query.TodoQuery;
import eu.smefinance.util.QuerydslUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.support.QuerydslRepositorySupport;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import java.util.Objects;

@Repository("todoRepositoryCustom")
public class TodoRepositoryCustomImpl extends QuerydslRepositorySupport implements TodoRepositoryCustom {

    public TodoRepositoryCustomImpl() {
        super(Todo.class);
    }

    @Override
    public Page<Todo> findTodos(@Nonnull final User user, @Nonnull final TodoQuery query, @Nonnull final Pageable pageable) {
        QTodo qTodo = QTodo.todo;

        BooleanBuilder predicate = new BooleanBuilder();
        predicate.and(qTodo.user.id.eq(user.getId()));

        if (StringUtils.isNotBlank(query.getTitle())) {
            predicate.and(qTodo.title.contains(query.getTitle()));
        }
        if (StringUtils.isNotBlank(query.getDescription())) {
            predicate.and(qTodo.description.contains(query.getDescription()));
        }
        if (query.getPriority() != null) {
            predicate.and(qTodo.priority.eq(query.getPriority()));
        }
        if (query.getDeadlineFrom() != null) {
            predicate.and(qTodo.deadline.goe(query.getDeadlineFrom()));
        }
        if (query.getDeadlineTo() != null) {
            predicate.and(qTodo.deadline.loe(query.getDeadlineTo()));
        }
        if (query.getReminderFrom() != null) {
            predicate.and(qTodo.reminder.goe(query.getReminderFrom()));
        }
        if (query.getReminderTo() != null) {
            predicate.and(qTodo.reminder.loe(query.getReminderTo()));
        }
        if (query.getCompleted() != null) {
            predicate.and(qTodo.completed.eq(query.getCompleted()));
        }
        if (query.getActive() != null) {
            predicate.and(qTodo.active.eq(query.getActive()));
        }
        if (query.getDeleted() != null) {
            predicate.and(qTodo.deleted.eq(query.getDeleted()));
        }

        return QuerydslUtil.getPageData(
                Objects.requireNonNull(getQuerydsl()),
                from(qTodo).where(predicate).orderBy(qTodo.priority.desc()).select(qTodo),
                pageable
        );
    }
}
