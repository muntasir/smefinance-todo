package eu.smefinance.repository.query;

import eu.smefinance.model.Priority;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@AllArgsConstructor
@Builder
@Getter
public class TodoQuery {
    private final Long userId;
    private final String title;
    private final String description;
    private final Priority priority;
    private final LocalDateTime deadlineFrom;
    private final LocalDateTime deadlineTo;
    private final LocalDateTime reminderFrom;
    private final LocalDateTime reminderTo;
    private final Boolean completed;
    private final Boolean active;
    private final Boolean deleted;
}
