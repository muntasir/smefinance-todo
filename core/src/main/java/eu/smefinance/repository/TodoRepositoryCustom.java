package eu.smefinance.repository;

import eu.smefinance.model.Todo;
import eu.smefinance.model.User;
import eu.smefinance.repository.query.TodoQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.annotation.Nonnull;

public interface TodoRepositoryCustom {

    Page<Todo> findTodos(@Nonnull final User user, @Nonnull final TodoQuery query, @Nonnull final Pageable pageable);
}
