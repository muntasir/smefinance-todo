package eu.smefinance.repository;

import eu.smefinance.model.Todo;
import eu.smefinance.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.annotation.Nonnull;
import java.util.Optional;

@Repository
public interface TodoRepository extends JpaRepository<Todo, Long>, TodoRepositoryCustom {
    Optional<Todo> findByUserAndId(@Nonnull User user, @Nonnull Long id);
}
