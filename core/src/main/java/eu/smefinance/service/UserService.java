package eu.smefinance.service;

import eu.smefinance.model.User;

import javax.annotation.Nonnull;
import java.util.Optional;

public interface UserService {

    Optional<User> findUser(@Nonnull final Long id);

    Optional<User> findUserByEmail(@Nonnull final String email);

    @Nonnull
    User getUser(@Nonnull final Long id);

    @Nonnull
    User getUserByEmail(@Nonnull final String email);

    @Nonnull
    User saveUser(@Nonnull final User user);
}
