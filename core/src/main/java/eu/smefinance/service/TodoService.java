package eu.smefinance.service;

import eu.smefinance.model.Todo;
import eu.smefinance.model.User;
import eu.smefinance.repository.query.TodoQuery;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.annotation.Nonnull;
import java.util.Optional;

public interface TodoService {

    Optional<Todo> findTodo(@Nonnull final User user, @Nonnull final Long id);

    @Nonnull
    Todo getTodo(@Nonnull final User user, @Nonnull final Long id);

    @Nonnull
    Page<Todo> getTodoList(@Nonnull final User user, @Nonnull final TodoQuery query, @Nonnull final Pageable pageable);

    @Nonnull
    Todo saveTodo(@Nonnull final User user, @Nonnull final Todo todo);

    void deleteTodo(@Nonnull final User user, @Nonnull final Todo todo);
}
