package eu.smefinance.service.impl;

import eu.smefinance.exception.EntityNotFoundException;
import eu.smefinance.exception.UserMismatchException;
import eu.smefinance.model.Todo;
import eu.smefinance.model.User;
import eu.smefinance.repository.TodoRepository;
import eu.smefinance.repository.query.TodoQuery;
import eu.smefinance.service.TodoService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class TodoServiceImpl implements TodoService {

    @Nonnull private final TodoRepository todoRepository;


    @Override
    public Optional<Todo> findTodo(@Nonnull final User user, @Nonnull final Long id) {
        return todoRepository.findByUserAndId(user, id);
    }

    @Nonnull
    @Override
    public Todo getTodo(@Nonnull final User user, @Nonnull final Long id) {
        return findTodo(user, id).orElseThrow(() -> new EntityNotFoundException(Todo.class));
    }

    @Nonnull
    @Override
    public Page<Todo> getTodoList(@Nonnull final User user, @Nonnull final TodoQuery query, @Nonnull final Pageable pageable) {
        return todoRepository.findTodos(user, query, pageable);
    }

    @Nonnull
    @Override
    @Transactional
    public Todo saveTodo(@Nonnull final User user, @Nonnull final Todo todo) {
        if (!user.equals(todo.getUser())) {
            throw new UserMismatchException(Todo.class);
        }
        return todoRepository.save(todo);
    }

    @Override
    @Transactional
    public void deleteTodo(@Nonnull final User user, @Nonnull final Todo todo) {
        todo.setDeleted(true);
        todoRepository.save(todo);
    }
}
