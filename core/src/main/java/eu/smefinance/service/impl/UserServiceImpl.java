package eu.smefinance.service.impl;

import eu.smefinance.exception.DuplicateException;
import eu.smefinance.exception.EntityNotFoundException;
import eu.smefinance.model.User;
import eu.smefinance.repository.UserRepository;
import eu.smefinance.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Nonnull;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

    @Nonnull private final UserRepository userRepository;


    @Override
    public Optional<User> findUser(@Nonnull final Long id) {
        return userRepository.findById(id);
    }

    @Override
    public Optional<User> findUserByEmail(@Nonnull final String email) {
        return userRepository.findByEmail(email);
    }

    @Nonnull
    @Override
    public User getUser(@Nonnull final Long id) {
        return findUser(id).orElseThrow(() -> new EntityNotFoundException(User.class));
    }

    @Nonnull
    @Override
    public User getUserByEmail(@Nonnull final String email) {
        return findUserByEmail(email).orElseThrow(() -> new EntityNotFoundException(User.class));
    }

    @Nonnull
    @Override
    @Transactional
    public User saveUser(@Nonnull final User user) {
        findUserByEmail(user.getEmail())
                .filter(existing -> !existing.equals(user))
                .ifPresent(existing -> {
                    throw new DuplicateException("Duplicate email address!");
                });

        return userRepository.save(user);
    }
}
