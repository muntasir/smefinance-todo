package eu.smefinance.model.converter;

import eu.smefinance.model.Priority;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class PriorityConverter implements AttributeConverter<Priority, Integer> {

    @Override
    public Integer convertToDatabaseColumn(Priority priority) {
        return priority == null ? null : priority.getValue();
    }

    @Override
    public Priority convertToEntityAttribute(Integer priorityValue) {
        return priorityValue == null ? null : Priority.getByValue(priorityValue);
    }
}
