package eu.smefinance.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.annotation.Nonnull;
import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum Priority {
    LOW(0),
    MEDIUM(1),
    HIGH(2);

    private Integer value;

    public static Priority getByValue(@Nonnull final Integer value) {
        return Arrays.stream(Priority.values())
                .filter(priority -> priority.getValue().equals(value))
                .findFirst()
                .orElse(null);
    }
}
