package eu.smefinance.api.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class LoginForm {
    private String email;
    private String password;
    private boolean rememberMe;
}
