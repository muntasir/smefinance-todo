package eu.smefinance.api.dto;

import lombok.*;

import java.time.ZonedDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class TodoDetails {
    private Long id;
    private UserDetails user;
    private String title;
    private String description;
    private String priority;
    private ZonedDateTime deadline;
    private ZonedDateTime reminder;
    private boolean completed;
    private boolean active;
}
