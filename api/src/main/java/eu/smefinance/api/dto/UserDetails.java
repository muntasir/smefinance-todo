package eu.smefinance.api.dto;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
public class UserDetails {
    private Long id;
    private String name;
    private String email;
    private String password;
    private boolean active;
}
