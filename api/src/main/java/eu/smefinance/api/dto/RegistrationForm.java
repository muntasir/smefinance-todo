package eu.smefinance.api.dto;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class RegistrationForm {
    private String name;
    private String email;
    private String password;
}
