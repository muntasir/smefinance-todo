package eu.smefinance.api.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
public class PageRequest {
    @Builder.Default private Integer pageNumber = 0;
    @Builder.Default private Integer pageSize = 10;
}
