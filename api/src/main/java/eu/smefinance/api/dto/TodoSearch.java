package eu.smefinance.api.dto;

import lombok.*;
import lombok.experimental.SuperBuilder;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@SuperBuilder
@Getter
@Setter
public class TodoSearch extends PageRequest {
    private String title;
    private String description;
    private String priority;
    private LocalDateTime deadlineFrom;
    private LocalDateTime deadlineTo;
    private LocalDateTime reminderFrom;
    private LocalDateTime reminderTo;
    private Boolean completed;
    private Boolean active;
    @Builder.Default private Boolean deleted = false;
}
