package eu.smefinance.api.configuration.properties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MailProperties {
    private String host;
    private Integer port;
    private String username;
    private String password;
    private boolean useAuth;
    private boolean startTls;
    private boolean debug;
}
