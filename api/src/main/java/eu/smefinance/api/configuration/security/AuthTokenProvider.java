package eu.smefinance.api.configuration.security;

import eu.smefinance.api.configuration.properties.SecurityProperties;
import eu.smefinance.util.DateTimeUtil;
import io.jsonwebtoken.*;
import io.jsonwebtoken.security.Keys;
import io.jsonwebtoken.security.SignatureException;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
@Log4j2
public class AuthTokenProvider implements InitializingBean {

    @Nonnull private final SecurityProperties securityProperties;
    private Key key;

    @Override
    public void afterPropertiesSet() {
        key = Keys.hmacShaKeyFor(securityProperties.getTokenSecret().getBytes(StandardCharsets.UTF_8));
    }


    public String createToken(@Nonnull final Authentication authentication, boolean rememberMe) {
        AuthUser authUser = (AuthUser) authentication.getPrincipal();
        return Jwts.builder()
                .setSubject(authUser.getUsername())
                .claim("auth", authUser.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.joining(",")))
                .signWith(key, SignatureAlgorithm.HS512)
                .setIssuedAt(new Date())
                .setExpiration(DateTimeUtil.toDate(LocalDateTime.now().plusMinutes(rememberMe ? securityProperties.getTokenValidityInMinutesForRememberMe() : securityProperties.getTokenValidityInMinutes())))
                .compact();
    }

    public String getUsernameFromToken(@Nonnull final String token) {
        try {
            return Jwts.parser().setSigningKey(key).parseClaimsJws(token).getBody().getSubject();
        } catch (ExpiredJwtException e) {
            log.error("JWT token is expired: {}", e.getMessage());
        } catch (UnsupportedJwtException e) {
            log.error("JWT token is unsupported: {}", e.getMessage());
        } catch (MalformedJwtException e) {
            log.error("Invalid JWT token: {}", e.getMessage());
        } catch (SignatureException e) {
            log.error("Invalid JWT signature: {}", e.getMessage());
        } catch (IllegalArgumentException e) {
            log.error("JWT claims string is empty: {}", e.getMessage());
        }
        return null;
    }
}
