package eu.smefinance.api.configuration;

import eu.smefinance.api.configuration.properties.DatabaseProperties;
import eu.smefinance.api.configuration.properties.MailProperties;
import eu.smefinance.api.configuration.properties.SecurityProperties;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class PropertiesConfiguration {

    @Bean
    @ConfigurationProperties(prefix = "smefinance.datasource")
    public DatabaseProperties databaseProperties() {
        return new DatabaseProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "smefinance.mail")
    public MailProperties mailProperties() {
        return new MailProperties();
    }

    @Bean
    @ConfigurationProperties(prefix = "smefinance.security")
    public SecurityProperties securityProperties() {
        return new SecurityProperties();
    }
}
