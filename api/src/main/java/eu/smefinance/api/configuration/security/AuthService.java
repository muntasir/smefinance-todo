package eu.smefinance.api.configuration.security;

import eu.smefinance.api.exception.UserInactiveException;
import eu.smefinance.model.User;
import eu.smefinance.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

@Component
@RequiredArgsConstructor
public class AuthService implements UserDetailsService {

    @Nonnull private final UserService userService;

    @Override
    public AuthUser loadUserByUsername(@Nonnull final String username) throws UsernameNotFoundException {
        final User user = userService.getUserByEmail(username);
        if (!user.isActive()) {
            throw new UserInactiveException("User<" + user.getEmail() + "> is inactive!");
        }
        return new AuthUser(user);
    }
}
