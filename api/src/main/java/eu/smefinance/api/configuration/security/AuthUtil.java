package eu.smefinance.api.configuration.security;

import eu.smefinance.model.User;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class AuthUtil {

    public User getLoggedInUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof AuthUser) {
            return ((AuthUser) principal).getUser();
        }
        throw new RuntimeException();
    }
}
