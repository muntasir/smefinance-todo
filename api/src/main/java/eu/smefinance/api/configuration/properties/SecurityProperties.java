package eu.smefinance.api.configuration.properties;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SecurityProperties {
    private String tokenSecret;
    private Integer tokenValidityInMinutes;
    private Integer tokenValidityInMinutesForRememberMe;
}
