package eu.smefinance.api.configuration;

import eu.smefinance.api.mapper.MapperRegistry;
import eu.smefinance.api.mapper.MapperRegistryImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfiguration {

    @Bean
    public MapperRegistry mapperRegistry() {
        return new MapperRegistryImpl();
    }
}
