package eu.smefinance.api.configuration.properties;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
public class DatabaseProperties {
    private String driverClassName;
    private String protocol;
    private String host;
    private Integer port;
    private String database;
    private String username;
    private String password;
    private String parameters;
    private Integer maximumPoolSize;

    public String getUrl() {
        return protocol + "//" + host + ":" + port + "/" + database + (StringUtils.isNotBlank(parameters) ? ("?" + parameters) : "");
    }
}
