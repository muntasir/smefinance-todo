package eu.smefinance.api.configuration.security;

import eu.smefinance.model.User;
import lombok.Getter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import javax.annotation.Nonnull;
import java.util.Set;

@Getter
public class AuthUser extends org.springframework.security.core.userdetails.User {

    private static final long serialVersionUID = -1773256050092806870L;
    private final User user;

    public AuthUser(@Nonnull final User user) {
        super(user.getEmail(), user.getPassword(), Set.of(new SimpleGrantedAuthority("USER")));
        this.user = user;
    }
}
