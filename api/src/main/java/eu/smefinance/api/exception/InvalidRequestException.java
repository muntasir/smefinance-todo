package eu.smefinance.api.exception;

import org.springframework.validation.ObjectError;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.stream.Collectors;

public class InvalidRequestException extends RuntimeException {

    private static final long serialVersionUID = 5424053247332717287L;
    private final List<ObjectError> errors;

    public InvalidRequestException(@Nonnull final List<ObjectError> objectErrors) {
        this.errors = objectErrors;
    }

    @Override
    public String getMessage() {
        return errors.stream().map(ObjectError::getDefaultMessage).collect(Collectors.joining("\n"));
    }
}
