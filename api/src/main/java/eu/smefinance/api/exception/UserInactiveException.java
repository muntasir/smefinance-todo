package eu.smefinance.api.exception;

import org.springframework.security.authentication.InternalAuthenticationServiceException;

public class UserInactiveException extends InternalAuthenticationServiceException {

    private static final long serialVersionUID = -631372434734967868L;

    public UserInactiveException(String message) {
        super(message);
    }
}
