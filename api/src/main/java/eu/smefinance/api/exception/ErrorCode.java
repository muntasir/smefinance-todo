package eu.smefinance.api.exception;

public enum ErrorCode {
    USER_NOT_FOUND,
    TODO_NOT_FOUND,

    USER_INACTIVE,

    INVALID_REQUEST,
    DUPLICATE_DATA,
    USER_MISMATCH
}
