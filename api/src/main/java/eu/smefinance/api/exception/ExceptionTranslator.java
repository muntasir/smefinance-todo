package eu.smefinance.api.exception;

import eu.smefinance.exception.DuplicateException;
import eu.smefinance.exception.EntityNotFoundException;
import eu.smefinance.exception.UserMismatchException;
import eu.smefinance.model.BaseEntity;
import eu.smefinance.model.Todo;
import eu.smefinance.model.User;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.NativeWebRequest;
import org.zalando.problem.Problem;
import org.zalando.problem.Status;
import org.zalando.problem.spring.web.advice.ProblemHandling;
import org.zalando.problem.spring.web.advice.security.SecurityAdviceTrait;

import java.util.HashMap;
import java.util.Map;

@ControllerAdvice
public class ExceptionTranslator implements ProblemHandling, SecurityAdviceTrait {

    private final Map<Class<? extends BaseEntity>, ErrorCode> notFoundErrorCodes;
    private final String ERROR_KEY = "error";

    public ExceptionTranslator() {
        notFoundErrorCodes = new HashMap<>();
        notFoundErrorCodes.put(User.class, ErrorCode.USER_NOT_FOUND);
        notFoundErrorCodes.put(Todo.class, ErrorCode.TODO_NOT_FOUND);
    }

    @Override
    public ResponseEntity<Problem> handleAuthentication(final AuthenticationException exception, final NativeWebRequest request) {
        if (exception instanceof UserInactiveException) {
            return create(
                    exception,
                    Problem.builder()
                            .withTitle(Status.UNAUTHORIZED.getReasonPhrase())
                            .withStatus(Status.UNAUTHORIZED)
                            .withDetail(exception.getMessage())
                            .with(ERROR_KEY, ErrorCode.USER_INACTIVE)
                            .build(),
                    request
            );
        }
        return create(exception, request);
    }


    @ExceptionHandler(DuplicateException.class)
    ResponseEntity<Problem> handleException(final DuplicateException exception, final NativeWebRequest request) {
        return create(
                exception,
                Problem.builder()
                        .withTitle(Status.BAD_REQUEST.getReasonPhrase())
                        .withStatus(Status.BAD_REQUEST)
                        .withDetail(exception.getMessage())
                        .with(ERROR_KEY, ErrorCode.DUPLICATE_DATA)
                        .build(),
                request
        );
    }

    @ExceptionHandler(EntityNotFoundException.class)
    ResponseEntity<Problem> handleException(final EntityNotFoundException exception, final NativeWebRequest request) {
        return create(
                exception,
                Problem.builder()
                        .withTitle(Status.NOT_FOUND.getReasonPhrase())
                        .withStatus(Status.NOT_FOUND)
                        .withDetail(exception.getMessage())
                        .with(ERROR_KEY, notFoundErrorCodes.get(exception.getClazz()))
                        .build(),
                request
        );
    }

    @ExceptionHandler(InvalidRequestException.class)
    ResponseEntity<Problem> handleException(final InvalidRequestException exception, final NativeWebRequest request) {
        return create(
                exception,
                Problem.builder()
                        .withTitle(Status.BAD_REQUEST.getReasonPhrase())
                        .withStatus(Status.BAD_REQUEST)
                        .withDetail(exception.getMessage())
                        .with(ERROR_KEY, ErrorCode.INVALID_REQUEST)
                        .build(),
                request
        );
    }

    @ExceptionHandler(UserMismatchException.class)
    ResponseEntity<Problem> handleException(final UserMismatchException exception, final NativeWebRequest request) {
        return create(
                exception,
                Problem.builder()
                        .withTitle(Status.BAD_REQUEST.getReasonPhrase())
                        .withStatus(Status.BAD_REQUEST)
                        .withDetail(exception.getMessage())
                        .with(ERROR_KEY, ErrorCode.USER_MISMATCH)
                        .build(),
                request
        );
    }
}
