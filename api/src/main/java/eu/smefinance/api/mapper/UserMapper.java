package eu.smefinance.api.mapper;

import eu.smefinance.api.dto.UserDetails;
import eu.smefinance.model.User;
import eu.smefinance.service.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class UserMapper {

    @Nonnull private final MapperRegistry mapperRegistry;
    @Nonnull private final UserService userService;
    @Nonnull private final PasswordEncoder passwordEncoder;

    @PostConstruct
    private void registerMappers() {
        mapperRegistry.addMapper(User.class, UserDetails.class, userToUserDetailsMapper());
        mapperRegistry.addMapper(UserDetails.class, User.class, userDetailsToUserMapper());
    }

    private Mapper<User, UserDetails> userToUserDetailsMapper() {
        return user -> UserDetails.builder()
                .id(user.getId())
                .name(user.getName())
                .email(user.getEmail())
                .active(user.isActive())
                .build();
    }

    private Mapper<UserDetails, User> userDetailsToUserMapper() {
        return details -> {
            User.UserBuilder<?, ?> userBuilder = details.getId() == null
                    ? User.builder()
                    : userService.getUser(details.getId()).toBuilder();

            userBuilder.name(details.getName())
                    .email(details.getEmail())
                    .active(details.isActive());

            if (StringUtils.isNotBlank(details.getPassword())) {
                userBuilder.password(passwordEncoder.encode(details.getPassword()));
            }

            return userBuilder.build();
        };
    }
}
