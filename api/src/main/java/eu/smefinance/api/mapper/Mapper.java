package eu.smefinance.api.mapper;

@FunctionalInterface
public interface Mapper<S, T> {
    T map(S source);
}
