package eu.smefinance.api.mapper;

import lombok.Getter;

@Getter
public class MapperAlreadyRegisteredException extends RuntimeException {

    private static final long serialVersionUID = 3235505504704538216L;
    private final Class<?> sourceClass;
    private final Class<?> targetClass;

    public MapperAlreadyRegisteredException(Class<?> sourceClass, Class<?> targetClass) {
        this.sourceClass = sourceClass;
        this.targetClass = targetClass;
    }

    @Override
    public String getMessage() {
        return "Mapper already registered for source class: " + sourceClass.getSimpleName() + " and target class: " + targetClass.getSimpleName();
    }
}
