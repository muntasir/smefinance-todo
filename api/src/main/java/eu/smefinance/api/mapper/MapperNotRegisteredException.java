package eu.smefinance.api.mapper;

import lombok.Getter;

@Getter
public class MapperNotRegisteredException extends RuntimeException {

    private static final long serialVersionUID = 3738016725292475032L;
    private final Class<?> sourceClass;
    private final Class<?> targetClass;

    public MapperNotRegisteredException(Class<?> sourceClass, Class<?> targetClass) {
        this.sourceClass = sourceClass;
        this.targetClass = targetClass;
    }

    @Override
    public String getMessage() {
        return "No Mapper registered for source class: " + sourceClass.getSimpleName() + " and target class: " + targetClass.getSimpleName();
    }
}
