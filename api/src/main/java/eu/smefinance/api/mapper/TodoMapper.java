package eu.smefinance.api.mapper;

import eu.smefinance.api.configuration.security.AuthUtil;
import eu.smefinance.api.dto.TodoDetails;
import eu.smefinance.api.dto.TodoSearch;
import eu.smefinance.api.dto.UserDetails;
import eu.smefinance.model.Priority;
import eu.smefinance.model.Todo;
import eu.smefinance.model.User;
import eu.smefinance.repository.query.TodoQuery;
import eu.smefinance.service.TodoService;
import eu.smefinance.util.DateTimeUtil;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.PostConstruct;

@Component
@RequiredArgsConstructor
public class TodoMapper {

    @Nonnull private final MapperRegistry mapperRegistry;
    @Nonnull private final TodoService todoService;
    @Nonnull private final AuthUtil authUtil;

    @PostConstruct
    private void registerMappers() {
        mapperRegistry.addMapper(Todo.class, TodoDetails.class, todoToTodoDetailsMapper());
        mapperRegistry.addMapper(TodoDetails.class, Todo.class, todoDetailsToTodoMapper());
        mapperRegistry.addMapper(TodoSearch.class, TodoQuery.class, todoSearchToTodoQueryMapper());
    }

    private Mapper<Todo, TodoDetails> todoToTodoDetailsMapper() {
        return todo -> TodoDetails.builder()
                .id(todo.getId())
                .user(mapperRegistry.getMapper(User.class, UserDetails.class).map(todo.getUser()))
                .title(todo.getTitle())
                .description(todo.getDescription())
                .priority(todo.getPriority().name())
                .deadline(todo.getDeadline() != null ? DateTimeUtil.toZonedDateTime(todo.getDeadline()) : null)
                .reminder(todo.getReminder() != null ? DateTimeUtil.toZonedDateTime(todo.getReminder()) : null)
                .completed(todo.isCompleted())
                .active(todo.isActive())
                .build();
    }

    private Mapper<TodoDetails, Todo> todoDetailsToTodoMapper() {
        return details -> {
            Todo.TodoBuilder<?, ?> todoBuilder = details.getId() == null
                    ? Todo.builder()
                    : todoService.getTodo(authUtil.getLoggedInUser(), details.getId()).toBuilder();

            todoBuilder.user(authUtil.getLoggedInUser())
                    .title(details.getTitle())
                    .description(details.getDescription())
                    .priority(Priority.valueOf(details.getPriority()))
                    .deadline(details.getDeadline() != null ? DateTimeUtil.toLocalDateTime(details.getDeadline()) : null)
                    .reminder(details.getReminder() != null ? DateTimeUtil.toLocalDateTime(details.getReminder()) : null)
                    .completed(details.isCompleted())
                    .active(details.isActive());

            return todoBuilder.build();
        };
    }

    private Mapper<TodoSearch, TodoQuery> todoSearchToTodoQueryMapper() {
        return search -> TodoQuery.builder()
                .title(search.getTitle())
                .description(search.getDescription())
                .priority(StringUtils.isNotBlank(search.getPriority()) ? Priority.valueOf(search.getPriority()) : null)
                .deadlineFrom(search.getDeadlineFrom())
                .deadlineTo(search.getDeadlineTo())
                .reminderFrom(search.getReminderFrom())
                .reminderTo(search.getReminderTo())
                .completed(search.getCompleted())
                .active(search.getActive())
                .deleted(search.getDeleted())
                .build();
    }
}
