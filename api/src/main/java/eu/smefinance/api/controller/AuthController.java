package eu.smefinance.api.controller;

import eu.smefinance.api.configuration.security.AuthTokenProvider;
import eu.smefinance.api.configuration.security.AuthUser;
import eu.smefinance.api.dto.AuthResponse;
import eu.smefinance.api.dto.LoginForm;
import eu.smefinance.api.dto.UserDetails;
import eu.smefinance.api.exception.InvalidRequestException;
import eu.smefinance.api.mapper.MapperRegistry;
import eu.smefinance.api.validator.LoginValidator;
import eu.smefinance.api.validator.RegistrationValidator;
import eu.smefinance.model.User;
import eu.smefinance.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Nonnull;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api")
@RequiredArgsConstructor
public class AuthController {

    @Nonnull private final LoginValidator loginValidator;
    @Nonnull private final RegistrationValidator registrationValidator;
    @Nonnull private final AuthTokenProvider authTokenProvider;
    @Nonnull private final AuthenticationManager authenticationManager;
    @Nonnull private final UserService userService;
    @Nonnull private final MapperRegistry mapperRegistry;


    @PostMapping(value = "/register", produces = APPLICATION_JSON_VALUE)
    public AuthResponse register(@RequestBody UserDetails userDetails, BindingResult result) {
        registrationValidator.validate(userDetails, result);
        if (result.hasErrors()) {
            throw new InvalidRequestException(result.getAllErrors());
        }
        final User user = userService.saveUser(mapperRegistry.getMapper(UserDetails.class, User.class).map(userDetails));
        final Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(userDetails.getEmail(), userDetails.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return AuthResponse.builder()
                .token(authTokenProvider.createToken(authentication, false))
                .user(mapperRegistry.getMapper(User.class, UserDetails.class).map(user))
                .build();
    }

    @PostMapping(value = "/login", produces = APPLICATION_JSON_VALUE)
    public AuthResponse login(@RequestBody LoginForm form, BindingResult result) {
        loginValidator.validate(form, result);
        if (result.hasErrors()) {
            throw new InvalidRequestException(result.getAllErrors());
        }
        final Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(form.getEmail(), form.getPassword()));
        final AuthUser authUser = (AuthUser) authentication.getPrincipal();
        SecurityContextHolder.getContext().setAuthentication(authentication);

        return AuthResponse.builder()
                .token(authTokenProvider.createToken(authentication, form.isRememberMe()))
                .user(mapperRegistry.getMapper(User.class, UserDetails.class).map(authUser.getUser()))
                .build();
    }
}
