package eu.smefinance.api.controller;

import eu.smefinance.api.configuration.security.AuthUtil;
import eu.smefinance.api.dto.TodoDetails;
import eu.smefinance.api.dto.TodoSearch;
import eu.smefinance.api.exception.InvalidRequestException;
import eu.smefinance.api.mapper.MapperRegistry;
import eu.smefinance.api.validator.TodoValidator;
import eu.smefinance.exception.UserMismatchException;
import eu.smefinance.model.Todo;
import eu.smefinance.model.User;
import eu.smefinance.repository.query.TodoQuery;
import eu.smefinance.service.TodoService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nonnull;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping("/api/todo")
@RequiredArgsConstructor
public class TodoController {

    @Nonnull private final TodoValidator todoValidator;
    @Nonnull private final AuthUtil authUtil;
    @Nonnull private final TodoService todoService;
    @Nonnull private final MapperRegistry mapperRegistry;


    @PostMapping(value = "/search", produces = APPLICATION_JSON_VALUE)
    public Page<TodoDetails> getTodoList(@RequestBody TodoSearch search) {
        return todoService.getTodoList(
                authUtil.getLoggedInUser(),
                mapperRegistry.getMapper(TodoSearch.class, TodoQuery.class).map(search),
                PageRequest.of(search.getPageNumber(), search.getPageSize())
        ).map(mapperRegistry.getMapper(Todo.class, TodoDetails.class)::map);
    }

    @GetMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public TodoDetails getTodoDetails(@PathVariable Long id) {
        return mapperRegistry.getMapper(Todo.class, TodoDetails.class).map(
                todoService.getTodo(authUtil.getLoggedInUser(), id)
        );
    }

    @PostMapping(produces = APPLICATION_JSON_VALUE)
    public TodoDetails saveTodo(@RequestBody TodoDetails details, BindingResult result) {
        todoValidator.validate(details, result);
        if (result.hasErrors()) {
            throw new InvalidRequestException(result.getAllErrors());
        }

        final User user = authUtil.getLoggedInUser();
        if (details.getId() != null) {
            final Todo todo = todoService.getTodo(user, details.getId());
            if (!todo.getUser().equals(user)) {
                throw new UserMismatchException(Todo.class);
            }
        }
        return mapperRegistry.getMapper(Todo.class, TodoDetails.class).map(
                todoService.saveTodo(user, mapperRegistry.getMapper(TodoDetails.class, Todo.class).map(details))
        );
    }

    @DeleteMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
    public Long deleteTodo(@PathVariable Long id) {
        final User user = authUtil.getLoggedInUser();

        final Todo todo = todoService.getTodo(user, id);
        if (!todo.getUser().equals(user)) {
            throw new UserMismatchException(Todo.class);
        }

        todoService.deleteTodo(user, todo);
        return todo.getId();
    }
}
