package eu.smefinance.api.validator;

import eu.smefinance.api.dto.TodoDetails;
import eu.smefinance.util.DateTimeUtil;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.annotation.Nonnull;
import java.time.LocalDateTime;

@Component
public class TodoValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(TodoDetails.class);
    }

    @Override
    public void validate(@Nonnull Object target, @Nonnull Errors errors) {
        TodoDetails details = (TodoDetails) target;

        if (StringUtils.isBlank(details.getTitle())) {
            errors.rejectValue("title", "title.is.required", "Title is required");
        }
        if (StringUtils.isBlank(details.getDescription())) {
            errors.rejectValue("description", "description.is.required", "Description is required");
        }
        if (details.getPriority() == null) {
            errors.rejectValue("priority", "priority.is.required", "Priority is required");
        }
        if (details.getDeadline() != null && DateTimeUtil.toLocalDateTime(details.getDeadline()).isBefore(LocalDateTime.now())) {
            errors.rejectValue("deadline", "deadline.is.required", "Invalid deadline");
        }
        if (details.getReminder() != null && (DateTimeUtil.toLocalDateTime(details.getDeadline()).isBefore(LocalDateTime.now()) || (details.getDeadline() != null && details.getReminder().isAfter(details.getDeadline())))) {
            errors.rejectValue("reminder", "reminder.is.required", "Invalid reminder");
        }
    }
}
