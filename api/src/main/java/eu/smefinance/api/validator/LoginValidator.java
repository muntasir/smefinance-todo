package eu.smefinance.api.validator;

import eu.smefinance.api.dto.LoginForm;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import javax.annotation.Nonnull;
import java.util.regex.Pattern;

@Component
public class LoginValidator implements Validator {

    public static final Pattern EMAIL_REGEX = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);
    public static final Pattern PASSWORD_REGEX = Pattern.compile("^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#&()–{}:;',?/*~$^+=<>]).{8,20}$");

    @Override
    public boolean supports(Class<?> clazz) {
        return clazz.equals(LoginForm.class);
    }

    @Override
    public void validate(@Nonnull Object target, @Nonnull Errors errors) {
        LoginForm form = (LoginForm) target;

        if (StringUtils.isBlank(form.getEmail())) {
            errors.rejectValue("email", "email.is.required", "Email is required");
        } else if (!EMAIL_REGEX.matcher(form.getEmail()).matches()) {
            errors.rejectValue("email", "email.is.invalid", "Invalid email address");
        }

        if (StringUtils.isBlank(form.getPassword())) {
            errors.rejectValue("password", "password.is.required", "Password is required");
        } else if (!PASSWORD_REGEX.matcher(form.getPassword()).matches()) {
            errors.rejectValue("password", "password.is.invalid", "Invalid password");
        }
    }
}
