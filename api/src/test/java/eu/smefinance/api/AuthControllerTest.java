package eu.smefinance.api;

import eu.smefinance.api.dto.LoginForm;
import eu.smefinance.api.dto.RegistrationForm;
import org.apache.commons.lang3.RandomStringUtils;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AuthControllerTest {

    private static final String REGISTER_ENDPOINT = "/register";
    private static final String LOGIN_ENDPOINT = "/login";


    @BeforeEach
    void setup() {
        TestUtil.setBaseURI();
    }

    @AfterEach
    void cleanup() {
        TestUtil.resetBaseURI();
    }

    private static RegistrationForm randomUser() {
        return RegistrationForm.builder()
                .name(RandomStringUtils.randomAlphabetic(100))
                .email(RandomStringUtils.randomAlphanumeric(20) + "@gmail.com")
                .password(RandomStringUtils.randomAlphanumeric(20) + "@" + RandomStringUtils.randomNumeric(5))
                .build();
    }


    @Test
    void register_emptyRequest_expectValidationError() {
        TestUtil.postRequest(REGISTER_ENDPOINT, null, RegistrationForm.builder().build())
                .then()
                .assertThat()
                .statusCode(400)
                .body("status", Matchers.equalTo("BAD_REQUEST"))
                .body("detail", Matchers.equalTo("Name is required\nEmail is required\nPassword is required"));
    }

    @Test
    void register_duplicateEmail_expectValidationError() {
        final RegistrationForm randomUser = randomUser();
        TestUtil.postRequest(REGISTER_ENDPOINT, null, randomUser);
        TestUtil.postRequest(REGISTER_ENDPOINT, null, randomUser)
                .then()
                .assertThat()
                .statusCode(400);
    }

    @Test
    void login_expectValidationError() {
        TestUtil.postRequest(LOGIN_ENDPOINT, null, LoginForm.builder().build())
                .then()
                .assertThat()
                .statusCode(400);
    }
}
