package eu.smefinance.api;

import eu.smefinance.api.dto.LoginForm;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

@Component
public class TestUtil {

    private static final String API_BASE_URI = "http://localhost:8080/api";
    private static final String LOGIN_ENDPOINT = "/login";
    private static final String USER1_EMAIL = "tester1@gmail.com";
    private static final String USER1_PASSWORD = "Tester#1";
    private static final String USER2_EMAIL = "tester2@gmail.com";
    private static final String USER2_PASSWORD = "Tester#2";
    private static final String TOKEN_KEY = "token";
    private static final String TOKEN_HEADER_KEY = "Authorization";
    private static final String TOKEN_PREFIX = "Bearer ";


    public static void setBaseURI() {
        RestAssured.baseURI = API_BASE_URI;
    }

    public static void resetBaseURI() {
        RestAssured.baseURI = null;
    }

    public static RequestSpecification getRequestSpecification() {
        return given().contentType(ContentType.JSON).log().all(true);
    }

    public static String login(@Nonnull final Integer user) {
        final Response response = getRequestSpecification().body(
                LoginForm.builder()
                        .email(user.equals(1) ? USER1_EMAIL : USER2_EMAIL)
                        .password(user.equals(1) ? USER1_PASSWORD : USER2_PASSWORD)
                        .rememberMe(false)
                        .build()
        ).post(LOGIN_ENDPOINT);
        assertThat(response.statusCode(), equalTo(200));
        JsonPath json = new JsonPath(response.body().asString());
        assertThat(json.get(TOKEN_KEY), notNullValue());
        return json.get(TOKEN_KEY);
    }

    public static Response postRequest(@Nonnull final String endPoint, final String token, final Object request) {
        return getRequestSpecification()
                .header(new Header(TOKEN_HEADER_KEY, TOKEN_PREFIX + token))
                .body(request)
                .post(endPoint);
    }
}
